"use strict";
const AWS = require("aws-sdk");

AWS.config.update({ region: "us-east-1" });

exports.handler = async (event, context, callback) => {
  const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });
  const documentClient = new AWS.DynamoDB.DocumentClient({
    region: "us-east-1"
  });

  // initialize variables to return data for api-gateway
  let responseBody = "";
  let statusCode = 0;

  // replace hardcoded id with variable user id - going to extract id from event
  const { id } = event.pathParameters;

  // describe item we want to retrieve with table name & primary key
  const params = {
    TableName: "Users",
    Key: {
      id: id
    }
  };

  // Return actual user data as a string from the id that we pass in the   
  // parameters. Response must consist of statuscode, headers, body and 
  // isBase64Encoded (binary-to-text). The responsebody will contain the user 
  // data if the request is a success, otherwise we will pass on an error 
  // message.
  try {
    const data = await documentClient.get(params).promise();
    responseBody = JSON.stringify(data.Item);
    statusCode = 200;
  } catch (err) {
    responseBody = `Unable to get user data`;
    statusCode = 403;
  }

  const response = {
    "statusCode": statusCode,
    "headers": {
      "testHeader": "test"
    },
    "body": responseBody,
    "isBase64Encoded": false
  };

  return response;
};

