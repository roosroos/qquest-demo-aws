"use strict";
const AWS = require("aws-sdk");

AWS.config.update({ region: "us-east-1" });

// event: information about event that triggers the function
// context: methods & properties with information about invocation & execution of function
// callback: callback method that can return to the calling function that invokes lambda

exports.handler = async (event, context) => {
  // retrieve item from DynamoDB

  // instantiate db - apiVersion is standard - en documentClient voor json-syntax
  const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });
  const documentClient = new AWS.DynamoDB.DocumentClient({
    region: "us-east-1",
  });

  // describe item we want to retrieve
  const params = {
    TableName: "Users",
    Key: {
      id: "1537",
    },
  };

  // callback function - if there is an error, log error, if no error, log the item (data)
  try {
    const data = await documentClient.get(params).promise();
    console.log(data);
  } catch (err) {
    console.log(err);
  }
};
