const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
  let statusCode = 0;
  let responseBody = '';

  // standard way to call S3 bucket
  const { name } = event.Records[0].s3.bucket;
  const { key } = event.Records[0].s3.object;
  
  const getObjectParams = {
    Bucket: name,
    Key: key
  };
  
  try {
    // get user data from s3
    const s3Data = await s3.getObject(getObjectParams).promise();
    const usersStr = s3Data.Body.toString();
    const usersJSON = JSON.parse(usersStr);
    console.log(`Users ::: ${usersStr}`);
        
    await Promise.all(usersJSON.map(async user => {
      const { id, firstName, lastName } = user;
      
      const putParams = {
        TableName: "Users",
        Item: {
          id: id,
          firstName: firstName,
          lastName: lastName
        }
      };
      
      await documentClient.put(putParams).promise();
      
    }));
    
    responseBody = 'Succeeded adding users';
    statusCode = 201;
      
  } catch(err) {
    console.log(err);
      responseBody = 'Error adding users';
      statusCode = 403;
  }
  
  const response = {
    statusCode: statusCode,
    body: responseBody
  };
  
  return response;
};
