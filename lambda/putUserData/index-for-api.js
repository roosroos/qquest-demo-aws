"use strict";
const AWS = require("aws-sdk");

AWS.config.update({ region: "us-east-1" });

exports.handler = async (event, context) => {
    const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });
    const documentClient = new AWS.DynamoDB.DocumentClient({
        region: "us-east-1"
    });

    let responseBody = "";
    let statusCode = 0;

    // the id, firstName & lastName should be read from the api-body
    const { id, firstName, lastName } = JSON.parse(event.body);

    // describe item we want to retrieve

  const params = {
    TableName: "Users",
    Item: {
      id: id,
      firstName: firstName,
      lastName: lastName
    }
  }

    // we're not returning anything, just posting data, but still want to 
    // see if success or not
  
 try {
    const data = await documentClient.put(params).promise();
    responseBody = JSON.stringify(data);
    statusCode = 201;
  } catch (err) {
    responseBody = `Unable to put user data`;
    statusCode = 403;
  }

  const response = {
    statusCode: statusCode,
    headers: {
      "testHeader": "test"
    },
    body: responseBody
  }

  return response;
}