"use strict";
const AWS = require("aws-sdk");

AWS.config.update({ region: "us-east-1" });

exports.handler = async (event, context) => {
  const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });
  const documentClient = new AWS.DynamoDB.DocumentClient({
    region: "us-east-1",
  });

  // describe item we want to put
  const params = {
    TableName: "Users",
    Item: {
      id: "1512",
      firstName: "Catharina",
      lastName: "Parr"
    }
  };


  // callback function with try catch block - if there is an error, log   
  // error, if there is no error, put & log the item data
  try {
    const data = await documentClient.put(params).promise();
    console.log(data);
  } catch (err) {
    console.log(err);
  }
};