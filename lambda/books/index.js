const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
    const params = {
        TableName: "Boeken",
        Item: {
            isbn: "9780802122551",
            title: "Euphoria",
            author: "Lily King"
        }
    };
    
    try {
        const response = await documentClient.put(params).promise();
        console.log(response);
    }
    catch (err) {
        console.log(err);
    }
};
